package moduls.sky;

import core.scene.GameObject;
import core.shaders.Shader;
import core.utils.ResourceLoader;;

public class AtmosphereShader extends Shader {
	private static AtmosphereShader instance = null;
	
	public static AtmosphereShader getInstance() {
		if(instance == null) {
			instance = new AtmosphereShader();
		}
		return instance;
	}
	
	protected AtmosphereShader() {
		super();
		
		addVertexShader(ResourceLoader.loadShader("shaders/atmosphere_VS.glsl"));
		addFragmentShader(ResourceLoader.loadShader("shaders/atmosphere_FS.glsl"));
		compileShader();
		
		addUniform("m_MVP");
		addUniform("m_World");
	}
	
	public void updateUniform(GameObject object) {
		setUniform("m_MVP", object.getWorldTransform().getMVP());
		setUniform("m_World", object.getWorldTransform().getWorldMatrix());
	}
}
